# Generated by Django 2.2.7 on 2019-12-24 18:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_contact'),
    ]

    operations = [
        migrations.AddField(
            model_name='recentwork',
            name='description',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
        migrations.AddField(
            model_name='recentwork',
            name='language_used',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
    ]
